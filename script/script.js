//Знайти всі параграфи на сторінці та встановити колір фону #ff0000
let paragraphs = document.querySelectorAll("p");
paragraphs.forEach(elem => {
    elem.style.backgroundColor = "#ff0000";
 });
 //Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. 
 //Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
let element = document.getElementById("optionsList");
console.log(element);
console.log(element.closest("div"));
if (element.hasChildNodes) {
    const nodes = element.childNodes;
    let text  = "";
    for (let i = 0; i < nodes.length; i++) {
      text += "Child node name: " + nodes[i].nodeName + " Child node type: " + nodes[i].nodeType + "\n";
    }
    console.log(text);
}
//Встановіть в якості контента елемента з класом testParagraph наступний параграф - 
//This is a paragraph
document.querySelector("#testParagraph").innerHTML = "<p>This is a paragraph</p>";

//Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль.
// Кожному з елементів присвоїти новий клас nav-item.
let elem = document.querySelector(".main-header").children;
console.log(elem);
for (let i = 0; i < elem.length; i++) {
    elem[i].classList.add("nav-item");
  }
//Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
let removeElements = document.querySelectorAll(".section-title");
for (let element of removeElements) {
    element.classList.remove("section-title");
}


